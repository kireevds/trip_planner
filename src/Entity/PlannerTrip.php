<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlannerTripRepository")
 */
class PlannerTrip
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TripPoint", mappedBy="trip")
     */
    private $points;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    public function __construct()
    {
        $this->points = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|TripPoint[]
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    public function addPoint(TripPoint $point): self
    {
        if (!$this->points->contains($point)) {
            $this->points[] = $point;
            $point->setTrip($this);
        }

        return $this;
    }

    public function removePoint(TripPoint $point): self
    {
        if ($this->points->contains($point)) {
            $this->points->removeElement($point);
            // set the owning side to null (unless already changed)
            if ($point->getTrip() === $this) {
                $point->setTrip(null);
            }
        }

        return $this;
    }

    public function toArray()
    {
        $points = [];
        foreach ($this->points as $point)
        {
            $points[] = $point->toArray();
        }
        return [
            "id" => $this->getId(),
            "city" => $this->getCity(),
            "date" => $this->getDate(),
            "rating" => $this->getRating(),
            "name" => $this->getName(),
            "points" => $points
        ];
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }
}
