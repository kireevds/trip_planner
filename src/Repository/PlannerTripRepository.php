<?php

namespace App\Repository;

use App\Entity\PlannerTrip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PlannerTrip|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlannerTrip|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlannerTrip[]    findAll()
 * @method PlannerTrip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlannerTripRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlannerTrip::class);
    }

    // /**
    //  * @return PlannerTrip[] Returns an array of PlannerTrip objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlannerTrip
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
