<?php
namespace App\Controller;

use App\Entity\TripPoint;
use App\Entity\PlannerTrip;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TripController extends AbstractController
{
    /**
     * @Route("/debug/")
     */
    public function debug()
    {
        return new Response(
            '<html><head><script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script></head><body>OK</body></html>'
        );
    }

    /**
     * @Route("/getTrip/{id}")
     * @param int $id
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTrip(int $id, EntityManagerInterface $em)
    {
        $trip = $em->find(PlannerTrip::class, $id);
        return $this->json($trip->toArray());
    }

    /**
     * @Route("/gtPTrip/")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function gtPTrip(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get('id');
        $trip = $em->find(PlannerTrip::class, $id);
        return $this->json($trip->toArray());
    }


    /**
     * @param array $point_data
     * @return TripPoint
     */
    private function preparePoint(array $point_data): TripPoint
    {
        // Создаем и заполняем объект точки
        $tripPoint = new TripPoint();
        $tripPoint
            ->setLat($point_data['lat'])
            ->setLng($point_data['lng'])
            ->setName($point_data['name'])
            ->setAddress($point_data['address']);
        return $tripPoint;
    }


    /**
     * @param EntityManagerInterface $em
     * @param PlannerTrip $trip
     */
    public function deleteExistingPoints(EntityManagerInterface $em, PlannerTrip $trip): void
    {
        // Удаляем те точки, которые привязаны к указанному путешествию
        $em->createQueryBuilder()
            ->delete()
            ->from(TripPoint::class, 'p')
            ->where('p.trip = :trip')
            ->setParameter('trip', $trip)
            ->getQuery()
            ->execute();
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param PlannerTrip $trip
     */
    public function updateTrip(Request $request, EntityManagerInterface $em, PlannerTrip $trip): void
    {
        // Установим параметры путешествия
        $trip->setCity($request->get('city'));
        $trip->setDate($request->get('date'));
        $trip->setName($request->get('name'));
        $trip->setRating($request->get('rating'));

        // Создадим новые точки путешествия
        $points = $request->get('points');
        foreach ($points as $point) {
            $tripPoint = $this->preparePoint($point);
            $trip->addPoint($tripPoint);
            $em->persist($tripPoint);
        }
        $em->persist($trip);
    }

    /**
     * @Route("/setRating/")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function setRating(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get('id');
        $trip = $em->find(PlannerTrip::class, $id);
        $trip->setRating($request->get('rating'));
        $em->flush();
        return $this->json(['trip_id' => $trip->getId(), 'newRating' => $trip->getRating()]);
    }

    /**
     * @Route("/saveTrip/")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveTrip(Request $request, EntityManagerInterface $em)
    {
        $id = $request->get('id');
        // Найдем интересующее нас путешествие

        if (!$id) {
            $trip = new PlannerTrip();
        } else {
            $trip = $em->find(PlannerTrip::class, $id);
            // Удалим существующие точки в путешествии
            $this->deleteExistingPoints($em, $trip);
        }

        // Обновим информацию о путешествии
        $this->updateTrip($request, $em, $trip);

        // Запишем изменения в БД
        $em->flush();

        return $this->json(['trip_id' => $trip->getId()]);
    }
}