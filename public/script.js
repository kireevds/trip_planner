$(function()
{
	// Функция ymaps.ready() будет вызвана, когда
    // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
	ymaps.ready(init);
    function init()
    {
        // Создание карты.
        window.myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [55.76, 37.64],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 5
        });
    }

	$('#searchButton').on('click', function()
	{
		var cityName = $('#cityName').val();
		if(cityName.length == 0) 
		{
			alert('Введите город, по которому вы хотите строить маршрут!');
			return;
		}
		
		//Отобразить на карте выбранный город
		var myGeocoder = ymaps.geocode(cityName);
		myGeocoder.then(function(res)
		{
			var geoObject = res.geoObjects.get(0);
			var coords = geoObject.geometry._coordinates;
			window.myMap.setCenter(coords, 9);
		});

		//Запрос через API Поиска по организациям
		var query = cityName + " достопримечательности";
		$.get('https://search-maps.yandex.ru/v1/?apikey=1a208090-4ed8-44c9-9831-f0a86bfe464b&text=' + query + '&lang=ru_RU&&results=100', function(response)
		{
			for(i in response.features)
			{
				var feature = response.features[i];
				var coords = feature.geometry.coordinates;
				var metaData = feature.properties.CompanyMetaData;

				var point = new ymaps.Placemark(coords.reverse(),
				{
		            balloonContentHeader: metaData.name,
		            balloonContentBody: '<a data-name="' + metaData.name + '" data-address="' + metaData.address + '" data-lat="' + coords[0] + '" data-lng="' + coords[1] + '" href="#" class="add-to-trip">Добавить в маршрут</a>',
		            balloonContentFooter: metaData.address,
		            hintContent: metaData.name
		        });

		        window.myMap.geoObjects.add(point);
			}
		});
	});

	$(document).on('click', '.add-to-trip', function()
		{
			document.getElementById("newTripEmpty").hidden = true;
			document.getElementById("newTripNotEmpty").hidden = false;
			var link = $(this);
			var newPoint = $('<li class="list-group-item">' + link.data('name') + '<div><a data-delname="' + link.data('name') + '" href="#" class="del-point"> x </a></div></li>');
			$("#newTrip ul").append(newPoint);
			newPoint.data(link.data());

			return false;
		});

	$(document).on('click', '.del-point', function()
	{
		var link = $(this);
		var deletename = link.data('delname');
		//Коряво, но работает. Не нашёл, можно ли передать ссылку на конкретный объект и удалить его
		$('#newTrip li').each(function()
			{
				var tripPoint = $(this);
				if (tripPoint.data().name == deletename) 
				{
					tripPoint.remove();
				}

			});
		
		return false;
	});

	$('#saveTripButton').on('click', function()
		{
			var tripRatingSelect = document.getElementById('rating');
    		var tripRating = tripRatingSelect.selectedIndex;
			var today = new Date();
			var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
			var tripName = $('#tripName').val();
			var cityName = $('#cityName').val();
			var points = [];
			$('#newTrip li').each(function()
			{
				var tripPoint = $(this);
				//Отсекаем div с "кнопкой" удаления поинтов
				if (tripPoint.data().name) 
				{
					points.push(tripPoint.data());
				}
			});

			var request = {
				'date' : date,
				'name' : tripName,
				'city' : cityName,
				'rating' : tripRating,
				'points' : points
			};

			console.log(request);
			$.post('/saveTrip/', request, function(response)
			{
				console.log(response);
				addNewTrip(tripName, response['trip_id']);
			});

			var addNewTrip = function(name, id)
			{
				$('#newTrip li').remove();
				document.getElementById("newTripEmpty").hidden = false;
				document.getElementById("newTripNotEmpty").hidden = true;
				

			    var table = document.getElementById("tripsTable");
			    table.hidden = false;
			    var rowCount = table.rows.length;
			    var row = table.insertRow(rowCount);
			    var newcell = row.insertCell(-1);
			    var newtripURL = '<a href="#" class="trip-item" data-id="' + id + '">' + name + '</a>'
			    newcell.innerHTML = newtripURL;
			    var newcell = row.insertCell(-1);
			    newcell.innerHTML = id;
			    var newcell = row.insertCell(-1);
			    var tableTripRate = '<select class="rating-select" id="' + id + '" name="newRating">' +
			    	'<option value="0">Без оценки</option>' +
					'<option value="1">1</option>' +
					'<option value="2">2</option>' +
					'<option value="3">3</option>' +
					'<option value="4">4</option>' +
					'<option value="5">5</option>' +
					'</select>';
			    newcell.innerHTML = tableTripRate;
			    document.getElementById(id).selectedIndex = tripRating;
				document.getElementById('rating').selectedIndex = '0';
				$('#myTrips i').hide();
				$('#tripName').val('');
			};

		});


	$(document).on('change', '.rating-select', function()
	{
		const newrating = this.value;
		const i = this.id;

		var request = {
				'id' : i,
				'rating' : newrating
			};

			$.post('/setRating/', request, function(response)
			{
				console.log(response);
			});
	});

$(document).on('click', '.trip-item', function()
	{
		var link = $(this);
		var getId = link.data('id');
		var request = {
				'id' : getId
			};

			$.post('/gtPTrip/', request, function(response)
			{
				console.log(response);
			});
	});

});